package es.urjc.code.practice.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@ToString
public class Airport {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(length = 3, unique = true, nullable = false)
    private String IATACode;

    @NotBlank
    private String name;

    @NotBlank
    private String city;

    @NotBlank
    private String country;

}
