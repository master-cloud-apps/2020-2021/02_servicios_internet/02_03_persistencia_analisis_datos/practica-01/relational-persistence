package es.urjc.code.practice.models;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(exclude = {"cabinCrew"})
public class Flight {

    public Flight(Long id, String flightCode, String companyName, Plane plane, Airport originAirport,
                  Airport destinationAirport, Date departureDate, Float durationTime) {
        this.id = id;
        this.flightCode = flightCode;
        this.companyName = companyName;
        this.plane = plane;
        this.originAirport = originAirport;
        this.destinationAirport = destinationAirport;
        this.departureDate = departureDate;
        this.durationTime = durationTime;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotBlank
    private String flightCode;

    @NotBlank
    private String companyName;

    @OneToOne
    @NotNull
    private Plane plane;

    @OneToOne
    @NotNull
    private Airport originAirport;

    @OneToOne
    @NotNull
    private Airport destinationAirport;

    @NotNull
    private Date departureDate;

    @NotNull
    private Float durationTime;

    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, orphanRemoval = true)
    @NotEmpty
    private List<FlightCabinCrew> cabinCrew;

}
