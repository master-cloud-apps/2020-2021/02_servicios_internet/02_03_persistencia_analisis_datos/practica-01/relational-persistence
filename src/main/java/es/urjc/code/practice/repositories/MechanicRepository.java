package es.urjc.code.practice.repositories;

import es.urjc.code.practice.models.Mechanic;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MechanicRepository extends JpaRepository<Mechanic, Long> {
}
