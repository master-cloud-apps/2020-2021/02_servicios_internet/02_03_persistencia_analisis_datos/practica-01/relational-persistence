package es.urjc.code.practice.dtos;

public interface CabinCrewWithTotalFlightsAndHoursIDTO {

    String getName();

    String getSurname();

    Long getTotalFlights();

    Float getTotalHours();

}
