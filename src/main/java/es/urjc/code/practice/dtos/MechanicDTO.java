package es.urjc.code.practice.dtos;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class MechanicDTO {

    private String name;
    private String surname;

}
